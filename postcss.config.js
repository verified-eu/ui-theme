
const tailwindNs = process.env.TAILWIND_NAMESPACE || 'verified'

module.exports = {
  plugins: [
     require('postcss-import'),
     require('tailwindcss')(`./tailwind.config.${tailwindNs}.js`),
     require('postcss-nested'),
     require('autoprefixer'),
  ]
}
