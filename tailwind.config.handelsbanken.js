
const merge = require("lodash/merge");
const config = require("./tailwind.config.verified");

module.exports = merge(config, {
  theme: {
    colors: {
      'transparent':    'transparent',
      'primary': {
        'default':      '#0B61A3',
        'dark':         '#00446C',
        'extra-dark':   '#065D90',
        'light-med':    '#64AEDB',
        'light':        '#B6D5E9',
        'extra-light':  '#DCF0FA',
        'green': '#B3EED2',
        'hb': '#5DAAE6',
        'hb-light': '#4A8DC0'
      },
      'secondary':      '#999999',
      'body':           '#444444',
      'background':     '#F4F4F4',
      'danger': {
        'default':      '#C43F30',
        'dark':         '#9F2A39',
        'extra-dark':   '#84212E',
        'light-med':    '#CC6673',
        'light':        '#F0D2D2',
        'extra-light':  '#FAE6E6'
      },
      'warning':        {
         'default':      '#EDC250',
         'dark':         '#CCA94C',
         'extra-dark':   '#AB8C3A',
         'light-med':    '#F2D484',
         'light':        '#EAD292',
         'extra-light':  '#F4E7C3'
      },
      'light':          '#E1E1E1',
      'muted':          '#D5D5D5',
      'white':          '#FFF'
    },
  },
});
