// ./plugins/gradients.js
const _ = require('lodash')

module.exports = function({ addUtilities, e, theme, variants }) {
  const breakpoint = theme('screens.sm', {})
  const input = theme('input', {})
  const hoverButton = theme('input.hoverButton', {})

  const utilities = {
    '.px-input': {
      paddingLeft: `${input.paddingInput}`,
      paddingRight: `${input.paddingInput}`
    },
    '.py-input': {
      paddingTop: `${input.paddingInput}`,
      paddingBottom: `${input.paddingInput}`
    },
    '.px-button': {
      paddingLeft: `${input.paddingButton}`,
      paddingRight: `${input.paddingButton}`
    },
    '.sm\\\:h-input': {
      height: `${input.heightDesktop.default}`,
      lineHeight: `${input.heightDesktop.default}`,
    },
    '.h-input': {
      height: `${input.heightMobile.default}`,
      lineHeight: `${input.heightMobile.default}`
    },
    '.sm\\\:h-input-sm': {
      height: `${input.heightDesktop.sm}`,
      lineHeight: `${input.heightDesktop.sm}`
    },
    '.h-input-sm': {
      height: `${input.heightMobile.sm}`,
      lineHeight: `${input.heightMobile.sm}`
    }
  }

  addUtilities(utilities)
}

