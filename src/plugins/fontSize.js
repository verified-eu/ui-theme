// ./plugins/gradients.js
const _ = require('lodash')

module.exports = function({ addUtilities, e, theme, variants }) {
  const breakpoint = theme('screens.sm', {})
  const fontSizeDesktop = theme('fontSize.desktop', {})
  const fontSizeMobile = theme('fontSize.mobile', {})

  const utilities = _.map(fontSizeMobile, (value, name) => ({
    [`.text-${e(name)}`]: {
      fontSize: `${value}`,
    },
  }))

  addUtilities(utilities)
}