// ./plugins/gradients.js
const _ = require('lodash')

module.exports = function({ addUtilities, e, theme, variants }) {
  const iconColor = theme('iconColor', {})

  const utilities = _.map(iconColor, (value, name) => ({
    [`.icon-${e(name)}`]: {
      width: `${value}`,
      height: `${value}`,
    }
  }))

  addUtilities(utilities)
}