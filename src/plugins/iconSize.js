// ./plugins/gradients.js
const _ = require('lodash')

module.exports = function({ addUtilities, e, theme, variants }) {
  const iconSize = theme('iconSize', {})

  const utilities = _.map(iconSize, (value, name) => ({
    [`.icon-${e(name)}`]: {
      width: `${value}`,
      height: `${value}`,
    }
  }))

  addUtilities(utilities)
}