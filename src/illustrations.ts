export interface PropsIllustrationSrc {
  file: any
  width: string
  height: string
}
export const uploadCloud:PropsIllustrationSrc = {
  file: require('./assets/illustrations/upload-cloud.svg'), 
  width: '88px',
  height: '74px'
};
export const documentUpload:PropsIllustrationSrc = {
  file: require('./assets/illustrations/document-upload.svg'),
  width: '87px',
  height: '113px'
};
export const documentZoom:PropsIllustrationSrc = {
  file: require('./assets/illustrations/document-zoom.svg'),
  width: '87px',
  height: '113px'
};
export const uploadEnvelope:PropsIllustrationSrc = {
  file: require('./assets/illustrations/-upload-envelope.svg'),
  width: '54px',
  height: '72px'
};
export const uploadEnvelope2:PropsIllustrationSrc = {
  file: require('./assets/illustrations/-upload-envelope-2.svg'),
  width: '54px',
  height: '76px'
};
export const uploadEnvelope2Enabled:PropsIllustrationSrc = {
  file: require('./assets/illustrations/-upload-envelope-2-enabled.svg'),
  width: '54px',
  height: '75px'
};
export const document:PropsIllustrationSrc = {
  file: require('./assets/illustrations/document.svg'),
  width: '74px',
  height: '106px'
};
export const uploadDocEnvelope:PropsIllustrationSrc = {
  file: require('./assets/illustrations/-upload-doc-envelope.svg'),
  width: '84px',
  height: '118px'
};
export const uploadDocEnvelopeEnabled:PropsIllustrationSrc = {
  file: require('./assets/illustrations/-upload-doc-envelope-enabled.svg'),
  width: '82px',
  height: '119px'
};
export const addRecipient:PropsIllustrationSrc = {
  file: require('./assets/illustrations/add-recipient.svg'),
  width: '101px',
  height: '100px'
};
export const sentEnvelope:PropsIllustrationSrc = {
  file: require('./assets/illustrations/sent-envelope.svg'),
  width: '122px',
  height: '160px'
};
