import * as icons from './src/icons';
import * as illustrations from './src/illustrations';
import * as spinners from './src/spinners';
import * as flagsLocale from './flagsLocale'
import * as flagsLang from './flagsLocale'



export { icons, illustrations, spinners} ;
export {flagsLocale, flagsLang}
