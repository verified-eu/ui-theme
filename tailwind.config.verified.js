const _ = require('lodash')
const plugin = require('tailwindcss/plugin')

module.exports = {
  prefix: '',
  important: false,
  separator: ':',
  theme: {
    screens: {
      'xs':             { default: '0px',     max: '639px' },
      'sm':             { default: '640px',   max: '767px' },
      'md':             { default: '768px',   max: '1023px' },
      'lg':             { default: '1024px',  max: '1193px' },
      'xl':             { default: '1194px', }
    },
    colors: {
      'transparent':    'transparent',
      'primary': {
        'default':      '#218BCB',
        'dark':         '#1478B4',
        'extra-dark':   '#065D90',
        'light-med':    '#64AEDB',
        'light':        '#B6D5E9',
        'extra-light':  '#DCF0FA'
      },
      'secondary':      '#999999',
      'body':           '#444444',
      'background':     '#F4F4F4',
      'danger': {
        'default':      '#B62537',
        'dark':         '#9F2A39',
        'extra-dark':   '#84212E',
        'light-med':    '#CC6673',
        'light':        '#F0D2D2',
        'extra-light':  '#FAE6E6'
      },
      'warning':        {
         'default':      '#EDC250',
         'dark':         '#CCA94C',
         'extra-dark':   '#AB8C3A',
         'light-med':    '#F2D484',
         'light':        '#EAD292',
         'extra-light':  '#F4E7C3'
      },
      'light':          '#E1E1E1',
      'muted':          '#D5D5D5',
      'white':          '#FFF'
    },
    spacing: {
      0:                '0px',
      1:                '1px',
      2:                '2px',
      4:                '4px',
      8:                '8px',
      12:               '12px',
      16:               '16px',
      20:               '20px',
      24:               '24px',
      32:               '32px',
      48:               '48px',
      64:               '64px'
    },
    backgroundColor: theme => theme('colors'),
    borderColor: theme => ({
      ...theme('colors')
    }),
    borderRadius: {
      0:                '0px',
      2:                '2px',
      4:                '4px',
      8:                '8px',
      12:               '12px',
      16:               '16px',
      'default':        '4px',
      'full':           '9999px'
    },
    borderWidth: {
      'default':        '1px',
      '0':              '0',
    },
    boxShadow: {
      'default':        '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
      'sm':             '1px 1px 2px rgba(166, 177, 198, 0.3)',
      'md':             '3px 3px 4px rgba(166, 177, 198, 0.2)',
      'lg':             '6px 6px 8px rgba(166, 177, 198, 0.3)',
      'none':           'none'
    },
    cursor: {
      'auto':           'auto',
      'default':        'default',
      'pointer':        'pointer',
      'wait':           'wait',
      'text':           'text',
      'move':           'move',
      'not-allowed':    'not-allowed'
    },
    flex: {
      1:                '1 1 0%',
      'auto':           '1 1 auto',
      'initial':        '0 1 auto',
      'none':           'none'
    },
    flexGrow: {
      0:                '0',
      'default':        '1'
    },
    flexShrink: {
      0:                '0',
      'default':        '1'
    },
    fontFamily: {
      'body':           '"Roboto", sans-serif',
      'heading':        '"Roboto Slab", serif'
    },
    fontSize: {
      'base':           {default: '14px', mobile: '15px'},
      'input':          {default: '14px', mobile: '14px'},
      'h1':             {default: '44px', mobile: '44px'},
      'h2':             {default: '26px', mobile: '26px'},
      'h3':             {default: '20px', mobile: '18px'},
      'h4':             {default: '18px', mobile: '19px'},
      'h5':             {default: '14px', mobile: '17px'},
      'h6':             {default: '12px', mobile: '14px'},
    },
    fontWeight: {
      'light':          '300',
      'normal':         '400',
      'medium':         '500',
      'semibold':       '600',
      'bold':           '700'
    },
    height: theme => ({
      ...theme('spacing'),
      '0':              '0',
      'input':          '42px',
      'input-compact':  '32px',
      'dropdown-item':  '34px',
      'navbar':         '48px',
      'auto':           'auto',
      'full':           '100%',
      'screen':         '100vh'
    }),
    iconSize: {
      'xs':             '8px',
      'sm':             '16px',
      'md':             '24px',
      'lg':             '32px',
      'xl':             '48px',
      'xxl':            '64px'
    },
    inset: theme => ({
      ...theme('spacing'),
      '0': '0',
      'auto': 'auto'
    }),
    letterSpacing: {
      'tighter':        '-0.05em',
      'tight':          '-0.025em',
      'normal':         '0',
      'wide':           '0.025em',
      'wider':          '0.05em',
      'widest':         '0.1em'
    },
    lineHeight: {
      '100':            '1',
      '120':            '1.2',
      '130':            '1.3',
      '140':            '1.4',
      '160':            '1.625',
      '200':            '2',
      'input':          '42px',
      'input-compact':  '32px',
      'default':        '1.4'
    },
    listStyleType: {
      'none':           'none'
    },
    margin: (theme, { negative }) => ({
      ...theme('spacing'),
      ...negative(theme('spacing')),
      '0': '0',
      'auto': 'auto',
    }),
    maxWidth: {
      'auto': 'auto',
      'dropdown': '250px',
      'full': '100%',
    },
    minWidth: theme => ({
      ...theme('width'),
      'btn': '100px',
      'dropdown': '180px'
   }),
    opacity: {
      '0': '0',
      '1': '1'
    },
    padding: (theme, { negative }) => ({
      ...theme('spacing'),
      '0': '0',
      'input': '12px',
      'input-compact': '8px',
      'button': '24px',
    }),
    textColor: theme => theme('colors'),
    width: theme => ({
      ...theme('spacing'),
      '0': '0',
      'auto': 'auto',
      '1/2': '50%',
      '1/3': '33.333333%',
      '2/3': '66.666667%',
      '1/4': '25%',
      '2/4': '50%',
      '3/4': '75%',
      '1/5': '20%',
      '2/5': '40%',
      '3/5': '60%',
      '4/5': '80%',
      '1/6': '16.666667%',
      '2/6': '33.333333%',
      '3/6': '50%',
      '4/6': '66.666667%',
      '5/6': '83.333333%',
      '1/12': '8.333333%',
      '2/12': '16.666667%',
      '3/12': '25%',
      '4/12': '33.333333%',
      '5/12': '41.666667%',
      '6/12': '50%',
      '7/12': '58.333333%',
      '8/12': '66.666667%',
      '9/12': '75%',
      '10/12': '83.333333%',
      '11/12': '91.666667%',
      'full': '100%',
      'screen': '100vw',
      'max-content': 'max-content'
    }),
    zIndex: {
      'dropdown': 1000,
      'modal': 1050
    },
  },
  variants: {
    alignItems: [],
    alignSelf: [],
    alignContent: [],
    appearance: [],
    backgroundColor: ['hover'],
    borderColor: ['hover'],
    borderRadius: [],
    borderStyle: [],
    borderWidth: [],
    boxShadow: [],
    boxSizing: [],
    boxBorder: [],
    clear: [],
    cursor: [],
    display: [],
    flex: [],
    flexDirection: [],
    flexGrow: [],
    flexShrink: [],
    flexWrap: [],
    float: [],
    fontFamily: [],
    fontSmoothing: [],
    fontStyle: [],
    fontWeight: [],
    height: [],
    inset: [],
    justifyContent: [],
    letterSpacing: [],
    lineHeight: [],
    listStyleType: [],
    margin: [],
    maxWidth: [],
    minHeight: [],
    minWidth: [],
    opacity: [],
    order: [],
    overflow: [],
    outline: false,
    padding: [],
    position: [],
    resize: [],
    textAlign: [],
    textColor: ['hover'],
    textDecoration: [],
    textTransform: [],
    userSelect: [],
    verticalAlign: [],
    visibility: [],
    whitespace: [],
    width: [],
    wordBreak: [],
    zIndex: []
  },
  corePlugins: {
    accessibility: false,
    animation: false,
    backgroundAttachment: false,
    backgroundPosition: false,
    backgroundRepeat: false,
    backgroundSize: false,
    borderCollapse: false,
    container: false,
    fill: false,
    fontSize: false,
    gap: false,
    gridAutoColumns: false,
    gridAutoFlow: false,
    gridAutoRows: false,
    gridColumn: false,
    gridColumnStart: false,
    gridColumnEnd: false,
    gridRow: false,
    gridRowStart: false,
    gridRowEnd: false,
    gridTemplateColumns: false,
    gridTemplateRows: false,
    listStylePosition: false,
    maxHeight: false,
    objectFit: false,
    objectPosition: false,
    placeholderColor: false,
    pointerEvents: false,
    rotate: false,
    scale: false,
    skew: false,
    stroke: false,
    strokeWidth: false,
    tableLayout: false,
    transform: false,
    transformOrigin: false,
    transition: false,
    transitionDelay: false,
    transitionDuration: false,
    transitionProperty: false,
    transitionTimingFunction: false,
    translate: false,
  },
  plugins: [
    plugin(function({ addUtilities, theme, e }) {
      const sizeUtilities = _.map(theme('spacing'), (value, key) => {
        return {
          [`.${e(`size-${key}`)}`]: {
            width: `${value}`,
            height: `${value}`,
          }
        }
      })
      const sizeFullUtilities = {
        '.size-full': {
          width: '100%',
          height: '100%'
        }
      }
      addUtilities(sizeUtilities)
      addUtilities(sizeFullUtilities)
    })
  ]
}