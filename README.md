[//]: # (block-start:title)

# Verified theme 2.0

[//]: # (block-stop:title)

[//]: # (block-start:summary)

Verified themed [Tailwind based css utilities](https://tailwindcss.com/) and assets.

[//]: # (block-stop:summary)


### Content 
This repo will build a list of .less files with tailwind utilities, and output in the /dist folder

- base.less - contains the css reset and base styles (background color, h1-h6 styles, etc); link this directly in your root html file
- utilities.THEME.less - includes the actual utilities; import this in your project and start using them


### How to regenerate the stylesheets

Run `npm run build` to generate the less files.

### Updating the themes

Make sure to add a version tag to your commit (and also in package.json)

- see previous commits for a reference