const fs = require('fs')
const ns = process.env.TAILWIND_NAMESPACE;
const content = fs.readFileSync(`./dist/base.${ns ? ns + '.': ''}css`, 'utf-8')
const result = `.base {\n${content}\n}`
fs.writeFileSync(`./dist/base.${ns ? ns + '.' : ''}less`, result)